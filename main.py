# -*- coding: utf-8 -*-
import os
from threading import Thread
from time import sleep
import json

import tornado.web
import tornado.httpserver
import sockjs.tornado


def send_messages():
    participants = TestConnection.participants
    while True:
        if participants:
            print participants
            conn = participants.copy().pop()
            conn.broadcast(participants,
                json.dumps(['Joe', 40, 10, 15])
            )
        sleep(20)


class Application(tornado.web.Application):
    def __init__(self):
        WebSocketRouter = sockjs.tornado.SockJSRouter(TestConnection, '/test')

        handlers = WebSocketRouter.urls + [
                    (r"/", MainHandler)
                ]

        settings = {
            "template_path": os.path.join(os.path.dirname(__file__), "template"),
            "static_path": os.path.join(os.path.dirname(__file__), "static"),
        }

        tornado.web.Application.__init__(self, handlers, **settings)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class TestConnection(sockjs.tornado.SockJSConnection):
    """Chat connection implementation"""
    # Class level variable
    participants = set()

    def on_open(self, info):
        # Send that someone joined
        # self.broadcast(self.participants, "Someone joined.")

        # Add client to the clients list
        self.participants.add(self)

    def on_message(self, message):
        # Broadcast message
        self.broadcast(self.participants, message)

    def on_close(self):
        # Remove client from the clients list and broadcast leave message
        self.participants.remove(self)

        # self.broadcast(self.participants, "Someone left.")


def main():
    applicaton = Application()
    th = Thread(target=send_messages)
    th.daemon = True
    th.start()
    http_server = tornado.httpserver.HTTPServer(applicaton)
    http_server.listen(8880)
    tornado.ioloop.IOLoop.instance().start()
    th.join()

if __name__ == "__main__":
    main()
