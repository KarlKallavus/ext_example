Ext.application({
    name   : 'MyApp',

    launch : function() {
        // var me = this;
        this.chatBox = Ext.widget({
            renderTo : Ext.getBody(),
            xtype    : 'grid',
            title    : 'Grid',
            width    : 650,
            height   : 300,
            plugins  : 'rowediting',
            store    : {
                fields : [ 'name', 'age', 'votes', 'credits' ],
                data   : [
                    [ 'Bill', 35, 10, 427 ],
                    [ 'Fred', 22, 4, 42 ]
                ]
            },
            columns: {
                defaults: {
                    editor : 'numberfield',
                    width  : 120
                },
                items: [
                    { text: 'Name', dataIndex: 'name', flex: 1, editor: 'textfield' },
                    { text: 'Age', dataIndex: 'age' },
                    { text: 'Votes', dataIndex: 'votes' },
                    { text: 'Credits', dataIndex: 'credits' }
                ]
            },

            connect : function() {
              // disconnect();
              var me = this;

              conn = new SockJS('http://' + window.location.host + '/test');

              conn.onopen = function() {
                alert('Connected.');
                // update_ui();
              };

              conn.onmessage = function(e) {
                alert('Received: ' + e.data);
                me.store.loadData([JSON.parse(e.data)], true);

                // log('Received: ' + e.data);
              };

              conn.onclose = function() {
                alert('Disconnected.');
                conn = null;
                //update_ui();
              };
            }
        });
        this.chatBox.connect();
    }
});
